<?php
include_once 'server_connection.php';
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Items With SN's Already In The Database</title>
    <style>
      table {
        border-collapse: collapse;
        width: 100%;
        color: #d96459;
        font-family: monospace;
        font-size: 25px;
        text-align: left;
      }
      table.center{
        margin-left: auto;
        margin-right: auto;
      }
      th{
        padding: 25px;
        background-color: #d96459;
        color: white;
      }
      td{
        padding: 25px;
      }
      tr: nth-child(even) {background-color: f2f2f2}
    </style>
  </head>
  <body>
    <table align="center">
      <tr>
        <th>id</th>
        <th>DCC Number</th>
        <th>Document Class</th>
        <th>SubClass text</th>
        <th>Origin Date</th>
        <th>Task Number</th>
        <th>TD Number</th>
        <th>Declass Date</th>
        <th>Document Status</th>
        <th>Document Status Eff Date</th>
        <th>DCC Number Repeat</th>
        <th>Agency Name</th>
        <th>DCC Last Modified On</th>
        <th>Logged Date</th>
        <th>Number of Copies</th>
        <th>Number of Parts</th>
        <th>Copy Number</th>
        <th>Copy Container</th>
        <th>Part Number</th>
        <th>Parts Container</th>
        <th>Copy Status</th>
        <th>Copy Status Eff Date</th>
        <th>Custodian</th>
        <th>Badge Number</th>
        <th>Parts Class</th>
        <th>Parts Status</th>
        <th>Parts Status Eff Date</th>
        <th>Parts Type</th>
        <th>Parts Description</th>
        <th>Archived Date</th>
        <th>Archived Approver</th>
        <th>Archived Agency</th>
        <th>Review Date</th>
        <th>Review Status</th>
        <th>SN</th>
        <?php

        $sql = "SELECT * FROM duplicate_repeat_sn_new";

        $result = $con-> query($sql);

        if ($result-> num_rows > 0) {
          while ($row = $result -> fetch_assoc()){
            echo "<tr><td>". $row["id"] . "</td><td>". $row["DCC_Number"] . "</td><td>". $row["Document_Class"] . "</td><td>". $row["SubClass"] . "</td><td>". $row["Origin_Date"] . "</td><td>". $row["Task_Number"] . "</td><td>". $row["TD_Number"] . "</td><td>". $row["Declass_Date"] . "</td><td>". $row["Document_Status"] . "</td><td>". $row["Doc_Status_Eff_Date"] . "</td><td>". $row["DCC_Number_Repeat"] . "</td><td>". $row["Agency_Name"] . "</td><td>". $row["DCC_Last_Modified_On"] . "</td><td>". $row["Logged_Date"] . "</td><td>". $row["Number_of_Copies"] . "</td><td>". $row["Number_of_Parts"] . "</td><td>". $row["Copy_Number"] . "</td><td>". $row["Copy_Container"] . "</td><td>". $row["Part_Number"] . "</td><td>". $row["Parts_Container"] . "</td><td>". $row["Copy_Status"] . "</td><td>". $row["Copy_Status_Eff_Date"] . "</td><td>". $row["Custodian"] . "</td><td>". $row["Badge_Number"] . "</td><td>". $row["Parts_Class"] . "</td><td>". $row["Parts_Status"] . "</td><td>". $row["Parts_Status_Eff_Date"] . "</td><td>". $row["Parts_Type"] . "</td><td>". $row["Parts_Description"] . "</td><td>". $row["Archived_Date"] . "</td><td>". $row["Archived_Approver"] . "</td><td>". $row["Archived_Agency"] . "</td><td>". $row["Review_Date"] . "</td><td>". $row["Review_Status"] . "</td><td>". $row["SN"] . "</td></tr>";
          }
          echo "</table>";
        }
        else {
          echo "0 result";
        }
        ?>
      </tr>
    </table>
  </body>
</html>
