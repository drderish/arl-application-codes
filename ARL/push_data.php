<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
    include_once 'server_connection.php';

    $sql_final_push = "SELECT * FROM permanent_data_temp";
    $result_final_push = mysqli_query($con,$sql_final_push);
    while ($row = mysqli_fetch_assoc($result_final_push)){
        $sql_post_final = "INSERT into permanent_data(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
          DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
          Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
          Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
          values ('".$row['DCC_Number']."','".$row['Document_Class']."','".$row['SubClass']."','".$row['Origin_Date']."','".$row['Task_Number']."','".$row['TD_Number']."','".$row['Declass_Date']."','".$row['Document_Status']."',
            '".$row['Doc_Status_Eff_Date']."','".$row['DCC_Number_Repeat']."','".$row['Agency_Name']."','".$row['DCC_Last_Modified_On']."','".$row['Logged_Date']."','".$row['Number_of_Copies']."','".$row['Number_of_Parts']."','".$row['Copy_Number']."','".$row['Copy_Container']."',
            '".$row['Part_Number']."','".$row['Parts_Container']."','".$row['Copy_Status']."','".$row['Copy_Status_Eff_Date']."','".$row['Custodian']."','".$row['Badge_Number']."','".$row['Parts_Class']."','".$row['Parts_Status']."','".$row['Parts_Status_Eff_Date']."',
            '".$row['Parts_Type']."','".$row['Parts_Description']."','".$row['Archived_Date']."','".$row['Archived_Approver']."','".$row['Archived_Agency']."','".$row['Review_Date']."','".$row['Review_Status']."','".$row['SN']."')";
            $result_post_final = mysqli_query($con,$sql_post_final);
    }

    if (isset($result_post_final)){
      echo "Data Was Submitted";
    }
    $table_names = ["complete_duplicates","csv_data","duplicate_dcc_sn_new","duplicate_repeat_dcc_new","duplicate_repeat_sn_new"
    ,"duplicate_sn_dcc_old","duplicate_sn_old","not_complete_duplicates","permanent_data_temp"];
    foreach($table_names as $table_name){
      $sql="TRUNCATE TABLE $table_name";
      mysqli_query($con,$sql);
    }
    ?>
    <form  action="index.php">
    <input type="submit" value="Submit Another CSV File">
    </form>
  </body>
</html>
