<?php
include_once 'server_connection.php';
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Choose Desired Data To See</title>
    <form  action="linesprocessed.php">
    <input type="submit" value="Items Submitted Via csv File">
    </form>
    <br>
    <form  action="duplicate_lines.php">
    <input type="submit" value="Submitted Items That Are Already In The Database">
    </form>
    <br>
    <form  action="new_dcc_and_SN.php">
    <input type="submit" value="Items That Will Be Added to The Database">
    </form>
    <br>
    <form  action="matching_dcc_but_not_SN.php">
    <input type="submit" value="Submitted Items With conflicting DCC Value's">
    </form>
    <br>
    <form  action="matching_SN_but_not_dcc.php">
    <input type="submit" value="Submitted Items With conflicting SN Value's">
    </form>
    <br>
    <form  action="update_exsisting_data.php">
    <input type="submit" value="Items That Aren't Exact Matches To Those In The Database, But Have The Same SN And DCC As An Item In The Database">
    </form>
    <br>
    <form  action="push_data.php">
    <input type="submit" value="Submit Items To The Database">
    </form>
    <br>
    <form  action="index.php">
    <input type="submit" value="Submit A Different CSV File">
    </form>
    <br>
  </head>
  <body>
    <?php
      // deleting all unecessary info from users csv submissions
      $sql = "SELECT * FROM csv_data;";
      $results = mysqli_query($con, $sql);
      $resultCheck = mysqli_num_rows($results);
      $check = true;
      if ($resultCheck > 0){
        while ($row = mysqli_fetch_assoc($results)){
           $temp = preg_replace('/\PL/u', '', $row['DCC_Number']);
           $temp_compare = $row['DCC_Number'];
           if ($check){
             mysqli_query($con,"DELETE FROM csv_data WHERE DCC_Number = '$temp_compare'");
           }
           if ($temp == "DCC")
           {
             $check = false;
           }
        }
      }

    //retrieving DCC and SN of the items newly submitted
    $sql2 = "SELECT * FROM csv_data;";
    $results2 = mysqli_query($con, $sql2);
    if (mysqli_num_rows($results2) > 0){
      $DCC_new = array();
      $SN_new = array();
      while ($row = mysqli_fetch_assoc($results2)){
        $DCC_new[] = $row['DCC_Number'];
        $SN_new[] = $row['SN'];
      }
    }

    //print_r($SN_new);
    //echo "<br>";
    //print_r($DCC_new);

    // retrieving all DCC and SN of current items in the database
    $sql3 = "SELECT * FROM permanent_data;";
    $results3 = mysqli_query($con, $sql3);
    if (mysqli_num_rows($results3) > 0){
      $DCC_old = array();
      $SN_old= array();
      while ($row = mysqli_fetch_assoc($results3)){
        $DCC_old[] = $row['DCC_Number'];
        $SN_old[] = $row['SN'];
      }
    }
    //echo "<br>";
    //print_r($DCC_old);
    //echo "<br>";
    //print_r($SN_old);


    //finding items that have the same SN or DDC as items already in database
    //also finds the position of the corresponding SN_old and stores this information
    //in $SN_old_position(does the same for DC_old). Now $duplicates_SN_counter and
    //$SN_old_position correspond to one another. $duplicate_DCC_counter and
    //$DC_old_position also correspond to one another.
    if (isset($SN_old)){
      if (count($SN_old) > 0){
        $duplicates_SN = array();
        $duplicates_SN_counter = array();
        $duplicates_DCC = array();
        $duplicates_DCC_counter = array();
        $counter = 0;
        $SN_old_position = array();
        foreach ($SN_new as $data){
          if (in_array($data,$SN_old)){
            $duplicates_SN[] = $data;
            $duplicates_SN_counter[] = $counter;
            $SN_old_position[] = array_search($data,$SN_old);
          }
          $counter ++;
        }
        $counter = 0;
        $DCC_old_position = array();
        foreach ($DCC_new as $data){
          if (in_array($data,$DCC_old)){
            $duplicates_DCC[] = $data;
            $duplicates_DCC_counter[] = $counter;
            $DCC_old_position[] = array_search($data,$DCC_old);
          }
          $counter ++;
        }
      }
    }
    //print_r($duplicates_SN);
    //print_r($duplicates_SN_counter);
    //print_r($SN_old_position);
    //print_r($duplicates_DCC);
    //print_r($duplicates_DCC_counter);
    //print_r($DCC_old_position);


    //getting the id primary key wrt the new data duplicate position arrays
    if(isset($duplicates_SN_counter) && isset($duplicates_DCC_counter)){
      if(count($duplicates_SN_counter) > 0 && count($duplicates_DCC_counter) > 0){
        $sql4 = "SELECT * FROM csv_data;";
        $results4 = mysqli_query($con, $sql4);
        $duplicate_SN_ids = array();
        $duplicate_DCC_ids = array();
        $counter = 0;
        while ($row = mysqli_fetch_assoc($results4)){
          if(in_array($counter,$duplicates_SN_counter)){
            $duplicate_SN_ids[] = $row['id'];
          }
          if(in_array($counter,$duplicates_DCC_counter)){
            $duplicates_DCC_ids[] = $row['id'];
          }
          $counter ++;
        }
      }
    }
    //print_r($duplicate_SN_ids);
    //print_r($duplicates_DCC_ids);

    //getting the id primary key wrt the old data duplicate position arrays
    if(isset($SN_old_position) && isset($DCC_old_position)){
      if(count($SN_old_position) > 0 && count($DCC_old_position) > 0){
        $sql5 = "SELECT * FROM permanent_data";
        $results5 = mysqli_query($con, $sql5);
        $duplicate_SN_ids_old = array();
        $duplicate_DCC_ids_old = array();
        $counter = 0;
        while ($row = mysqli_fetch_assoc($results5)){
          if(in_array($counter,$SN_old_position)){
            $duplicate_SN_ids_old[] = $row['id'];
          }
          if(in_array($counter,$DCC_old_position)){
            $duplicate_DCC_ids_old[] = $row['id'];
          }
          $counter ++;
        }
      }
    }
    //print_r($duplicate_SN_ids_old);
    //print_r($duplicate_DCC_ids_old);

    //putting all new duplicate sn items into a mysql table
    if (isset($duplicate_SN_ids)){
      if (count($duplicate_SN_ids)>0){
        $sql_SN_new = "SELECT * FROM csv_data";
        $results_SN_new = mysqli_query($con,$sql_SN_new);
        while ($row = mysqli_fetch_assoc($results_SN_new)){
          if (in_array($row['id'],$duplicate_SN_ids)){
            $sql_post_new_SN = "INSERT into duplicate_sn_new(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
              DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
              Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
              Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
              values ('".$row['DCC_Number']."','".$row['Document_Class']."','".$row['SubClass']."','".$row['Origin_Date']."','".$row['Task_Number']."','".$row['TD_Number']."','".$row['Declass_Date']."','".$row['Document_Status']."',
                '".$row['Doc_Status_Eff_Date']."','".$row['DCC_Number_Repeat']."','".$row['Agency_Name']."','".$row['DCC_Last_Modified_On']."','".$row['Logged_Date']."','".$row['Number_of_Copies']."','".$row['Number_of_Parts']."','".$row['Copy_Number']."','".$row['Copy_Container']."',
                '".$row['Part_Number']."','".$row['Parts_Container']."','".$row['Copy_Status']."','".$row['Copy_Status_Eff_Date']."','".$row['Custodian']."','".$row['Badge_Number']."','".$row['Parts_Class']."','".$row['Parts_Status']."','".$row['Parts_Status_Eff_Date']."',
                '".$row['Parts_Type']."','".$row['Parts_Description']."','".$row['Archived_Date']."','".$row['Archived_Approver']."','".$row['Archived_Agency']."','".$row['Review_Date']."','".$row['Review_Status']."','".$row['SN']."')";
                $result_post_new_SN = mysqli_query($con,$sql_post_new_SN);
          }
        }
      }
    }

    //putting all old duplicate sn items into a mysql table
    if (isset($duplicate_SN_ids_old)){
      if (count($duplicate_SN_ids_old)>0){
        $sql_SN_old = "SELECT * FROM permanent_data";
        $results_SN_old = mysqli_query($con,$sql_SN_old);
        while ($row = mysqli_fetch_assoc($results_SN_old)){
          if (in_array($row['id'],$duplicate_SN_ids_old)){
            $sql_post_SN_old = "INSERT into duplicate_sn_old(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
              DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
              Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
              Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
              values ('".$row['DCC_Number']."','".$row['Document_Class']."','".$row['SubClass']."','".$row['Origin_Date']."','".$row['Task_Number']."','".$row['TD_Number']."','".$row['Declass_Date']."','".$row['Document_Status']."',
                '".$row['Doc_Status_Eff_Date']."','".$row['DCC_Number_Repeat']."','".$row['Agency_Name']."','".$row['DCC_Last_Modified_On']."','".$row['Logged_Date']."','".$row['Number_of_Copies']."','".$row['Number_of_Parts']."','".$row['Copy_Number']."','".$row['Copy_Container']."',
                '".$row['Part_Number']."','".$row['Parts_Container']."','".$row['Copy_Status']."','".$row['Copy_Status_Eff_Date']."','".$row['Custodian']."','".$row['Badge_Number']."','".$row['Parts_Class']."','".$row['Parts_Status']."','".$row['Parts_Status_Eff_Date']."',
                '".$row['Parts_Type']."','".$row['Parts_Description']."','".$row['Archived_Date']."','".$row['Archived_Approver']."','".$row['Archived_Agency']."','".$row['Review_Date']."','".$row['Review_Status']."','".$row['SN']."')";
                $result_post_SN_old = mysqli_query($con,$sql_post_SN_old);
          }
        }
      }
    }

    //putting new duplicate dcc without duplicate SN into a table
    $duplicate_repeat_sn_dcc_new = array();
    if (isset($duplicate_DCC_ids) && isset($duplicate_SN_ids)){
      if (count($duplicate_DCC_ids)>0 && count($duplicate_SN_ids)>0){
        $sql_repeat_DCC_new = "SELECT * FROM csv_data";
        $results_repeat_DCC_new = mysqli_query($con,$sql_repeat_DCC_new);
        while ($row = mysqli_fetch_assoc($results_repeat_DCC_new)){
          if (in_array($row['id'],$duplicate_DCC_ids) && !in_array($row['id'],$duplicate_SN_ids)){
            $sql_post_repeat_DCC_new = "INSERT into duplicate_repeat_dcc_new(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
              DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
              Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
              Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
              values ('".$row['DCC_Number']."','".$row['Document_Class']."','".$row['SubClass']."','".$row['Origin_Date']."','".$row['Task_Number']."','".$row['TD_Number']."','".$row['Declass_Date']."','".$row['Document_Status']."',
                '".$row['Doc_Status_Eff_Date']."','".$row['DCC_Number_Repeat']."','".$row['Agency_Name']."','".$row['DCC_Last_Modified_On']."','".$row['Logged_Date']."','".$row['Number_of_Copies']."','".$row['Number_of_Parts']."','".$row['Copy_Number']."','".$row['Copy_Container']."',
                '".$row['Part_Number']."','".$row['Parts_Container']."','".$row['Copy_Status']."','".$row['Copy_Status_Eff_Date']."','".$row['Custodian']."','".$row['Badge_Number']."','".$row['Parts_Class']."','".$row['Parts_Status']."','".$row['Parts_Status_Eff_Date']."',
                '".$row['Parts_Type']."','".$row['Parts_Description']."','".$row['Archived_Date']."','".$row['Archived_Approver']."','".$row['Archived_Agency']."','".$row['Review_Date']."','".$row['Review_Status']."','".$row['SN']."')";
                $result_post_repeat_DCC_new = mysqli_query($con,$sql_post_repeat_DCC_new);
                $duplicate_repeat_sn_dcc_new[] = $row['SN'];
          }
        }
      }
    }

    //putting all new data with duplicate dcc and sn into a mysql table. data with repeat sn but non repeate dcc is stored in a seperate table.
    $duplicate_repeat_sn_sn_new = array();
    if (isset($sql_post_new_SN)){
      $sql_select_SN_new = "SELECT * FROM duplicate_sn_new";
      $result_select_SN_new = mysqli_query($con,$sql_select_SN_new);
      $repeat_sn_values_new = array();
      while ($row = mysqli_fetch_assoc($result_select_SN_new)){
        if (in_array($row['DCC_Number'],$duplicates_DCC)){
          $sql_post_DCC_SN_new = "INSERT into duplicate_dcc_sn_new(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
            DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
            Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
            Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
            values ('".$row['DCC_Number']."','".$row['Document_Class']."','".$row['SubClass']."','".$row['Origin_Date']."','".$row['Task_Number']."','".$row['TD_Number']."','".$row['Declass_Date']."','".$row['Document_Status']."',
              '".$row['Doc_Status_Eff_Date']."','".$row['DCC_Number_Repeat']."','".$row['Agency_Name']."','".$row['DCC_Last_Modified_On']."','".$row['Logged_Date']."','".$row['Number_of_Copies']."','".$row['Number_of_Parts']."','".$row['Copy_Number']."','".$row['Copy_Container']."',
              '".$row['Part_Number']."','".$row['Parts_Container']."','".$row['Copy_Status']."','".$row['Copy_Status_Eff_Date']."','".$row['Custodian']."','".$row['Badge_Number']."','".$row['Parts_Class']."','".$row['Parts_Status']."','".$row['Parts_Status_Eff_Date']."',
              '".$row['Parts_Type']."','".$row['Parts_Description']."','".$row['Archived_Date']."','".$row['Archived_Approver']."','".$row['Archived_Agency']."','".$row['Review_Date']."','".$row['Review_Status']."','".$row['SN']."')";
              $result_post_DCC_SN_new = mysqli_query($con,$sql_post_DCC_SN_new);
              $repeat_sn_values_new[] = $row['SN'];
        }else{
          $sql_post_repeat_SN_new = "INSERT into duplicate_repeat_sn_new(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
            DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
            Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
            Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
            values ('".$row['DCC_Number']."','".$row['Document_Class']."','".$row['SubClass']."','".$row['Origin_Date']."','".$row['Task_Number']."','".$row['TD_Number']."','".$row['Declass_Date']."','".$row['Document_Status']."',
              '".$row['Doc_Status_Eff_Date']."','".$row['DCC_Number_Repeat']."','".$row['Agency_Name']."','".$row['DCC_Last_Modified_On']."','".$row['Logged_Date']."','".$row['Number_of_Copies']."','".$row['Number_of_Parts']."','".$row['Copy_Number']."','".$row['Copy_Container']."',
              '".$row['Part_Number']."','".$row['Parts_Container']."','".$row['Copy_Status']."','".$row['Copy_Status_Eff_Date']."','".$row['Custodian']."','".$row['Badge_Number']."','".$row['Parts_Class']."','".$row['Parts_Status']."','".$row['Parts_Status_Eff_Date']."',
              '".$row['Parts_Type']."','".$row['Parts_Description']."','".$row['Archived_Date']."','".$row['Archived_Approver']."','".$row['Archived_Agency']."','".$row['Review_Date']."','".$row['Review_Status']."','".$row['SN']."')";
              $result_post_repeat_SN_new = mysqli_query($con,$sql_post_repeat_SN_new);
              $duplicate_repeat_sn_sn_new[] = $row['SN'];
        }
      }
    }

    //need to pull all old data with matching sn and dcc wrt duplicate_dcc_sn_new and store this information in a mysql table
    if (isset($repeat_sn_values_new)){
      if (count($repeat_sn_values_new)>0){
        $sql_pull_SN_old = "SELECT * FROM permanent_data";
        $result_pull_SN_old = mysqli_query($con,$sql_pull_SN_old);
        while ($row = mysqli_fetch_assoc($result_pull_SN_old)){
          if(in_array($row['SN'],$repeat_sn_values_new)){
          $sql_post_SN_DCC_duplicates_old = "INSERT into duplicate_sn_dcc_old(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
            DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
            Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
            Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
            values ('".$row['DCC_Number']."','".$row['Document_Class']."','".$row['SubClass']."','".$row['Origin_Date']."','".$row['Task_Number']."','".$row['TD_Number']."','".$row['Declass_Date']."','".$row['Document_Status']."',
              '".$row['Doc_Status_Eff_Date']."','".$row['DCC_Number_Repeat']."','".$row['Agency_Name']."','".$row['DCC_Last_Modified_On']."','".$row['Logged_Date']."','".$row['Number_of_Copies']."','".$row['Number_of_Parts']."','".$row['Copy_Number']."','".$row['Copy_Container']."',
              '".$row['Part_Number']."','".$row['Parts_Container']."','".$row['Copy_Status']."','".$row['Copy_Status_Eff_Date']."','".$row['Custodian']."','".$row['Badge_Number']."','".$row['Parts_Class']."','".$row['Parts_Status']."','".$row['Parts_Status_Eff_Date']."',
              '".$row['Parts_Type']."','".$row['Parts_Description']."','".$row['Archived_Date']."','".$row['Archived_Approver']."','".$row['Archived_Agency']."','".$row['Review_Date']."','".$row['Review_Status']."','".$row['SN']."')";
              $result_post_SN_DCC_duplicates_old = mysqli_query($con,$sql_post_SN_DCC_duplicates_old);
            }
          }
        }
      }

    //need to see if items in duplicate_dcc_sn_new table are complete duplicates or not, if complete duplicates, they will not be added to permanent data
    //can make this process more computationally efficient
    $complete_duplicate_sn_values = array();
    if (isset($repeat_sn_values_new)){
      if (count($repeat_sn_values_new)>0){
        $sql_new = "SELECT * FROM duplicate_dcc_sn_new";
        $result_new = mysqli_query($con,$sql_new);
        $counter = 0;
        while ($row1 = mysqli_fetch_assoc($result_new)){
          $sql_old = "SELECT * FROM duplicate_sn_dcc_old";
          $result_old = mysqli_query($con,$sql_old);
          while ($row2 = mysqli_fetch_assoc($result_old)){
            if ($row1['SN'] == $row2['SN']){
              $data1 = $row1['DCC_Number'].$row1['Document_Class'].$row1['SubClass'].$row1['Origin_Date'].$row1['Task_Number'].$row1['TD_Number'].$row1['Declass_Date'].$row1['Document_Status'].
                $row1['Doc_Status_Eff_Date'].$row1['DCC_Number_Repeat'].$row1['Agency_Name'].$row1['DCC_Last_Modified_On'].$row1['Logged_Date'].$row1['Number_of_Copies'].$row1['Number_of_Parts'].$row1['Copy_Number'].$row1['Copy_Container']
                .$row1['Part_Number'].$row1['Parts_Container'].$row1['Copy_Status'].$row1['Copy_Status_Eff_Date'].$row1['Custodian'].$row1['Badge_Number'].$row1['Parts_Class'].$row1['Parts_Status'].$row1['Parts_Status_Eff_Date'].
                $row1['Parts_Type'].$row1['Parts_Description'].$row1['Archived_Date'].$row1['Archived_Approver'].$row1['Archived_Agency'].$row1['Review_Date'].$row1['Review_Status'].$row1['SN'];
              $data2 = $row2['DCC_Number'].$row2['Document_Class'].$row2['SubClass'].$row2['Origin_Date'].$row2['Task_Number'].$row2['TD_Number'].$row2['Declass_Date'].$row2['Document_Status'].
                $row2['Doc_Status_Eff_Date'].$row2['DCC_Number_Repeat'].$row2['Agency_Name'].$row2['DCC_Last_Modified_On'].$row2['Logged_Date'].$row2['Number_of_Copies'].$row2['Number_of_Parts'].$row2['Copy_Number'].$row2['Copy_Container']
                .$row2['Part_Number'].$row2['Parts_Container'].$row2['Copy_Status'].$row2['Copy_Status_Eff_Date'].$row2['Custodian'].$row2['Badge_Number'].$row2['Parts_Class'].$row2['Parts_Status'].$row2['Parts_Status_Eff_Date'].
                $row2['Parts_Type'].$row2['Parts_Description'].$row2['Archived_Date'].$row2['Archived_Approver'].$row2['Archived_Agency'].$row2['Review_Date'].$row2['Review_Status'].$row2['SN'];
                if ($data1 == $data2){
                    $sql_post_complete_duplicate = "INSERT into complete_duplicates(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
                      DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
                      Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
                      Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
                      values ('".$row1['DCC_Number']."','".$row1['Document_Class']."','".$row1['SubClass']."','".$row1['Origin_Date']."','".$row1['Task_Number']."','".$row1['TD_Number']."','".$row1['Declass_Date']."','".$row1['Document_Status']."',
                        '".$row1['Doc_Status_Eff_Date']."','".$row1['DCC_Number_Repeat']."','".$row1['Agency_Name']."','".$row1['DCC_Last_Modified_On']."','".$row1['Logged_Date']."','".$row1['Number_of_Copies']."','".$row1['Number_of_Parts']."','".$row1['Copy_Number']."','".$row1['Copy_Container']."',
                        '".$row1['Part_Number']."','".$row1['Parts_Container']."','".$row1['Copy_Status']."','".$row1['Copy_Status_Eff_Date']."','".$row1['Custodian']."','".$row1['Badge_Number']."','".$row1['Parts_Class']."','".$row1['Parts_Status']."','".$row1['Parts_Status_Eff_Date']."',
                        '".$row1['Parts_Type']."','".$row1['Parts_Description']."','".$row1['Archived_Date']."','".$row1['Archived_Approver']."','".$row1['Archived_Agency']."','".$row1['Review_Date']."','".$row1['Review_Status']."','".$row1['SN']."')";
                        $result_post_complete_duplicates = mysqli_query($con,$sql_post_complete_duplicate);
                        $complete_duplicate_sn_values[] = $row1['SN'];
                  }else{
                    $empty = "_";
                    $sql_post_not_complete_duplicate_space = "INSERT into not_complete_duplicates(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
                      DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
                      Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
                      Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
                      values ('"."NEW ITEM"."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                      '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                      '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                      '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."')";
                      $result_post_not_complete_duplicates_space = mysqli_query($con,$sql_post_not_complete_duplicate_space);

                      $sql_post_not_complete_duplicate = "INSERT into not_complete_duplicates(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
                      DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
                      Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
                      Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
                      values ('".$row1['DCC_Number']."','".$row1['Document_Class']."','".$row1['SubClass']."','".$row1['Origin_Date']."','".$row1['Task_Number']."','".$row1['TD_Number']."','".$row1['Declass_Date']."','".$row1['Document_Status']."',
                      '".$row1['Doc_Status_Eff_Date']."','".$row1['DCC_Number_Repeat']."','".$row1['Agency_Name']."','".$row1['DCC_Last_Modified_On']."','".$row1['Logged_Date']."','".$row1['Number_of_Copies']."','".$row1['Number_of_Parts']."','".$row1['Copy_Number']."','".$row1['Copy_Container']."',
                      '".$row1['Part_Number']."','".$row1['Parts_Container']."','".$row1['Copy_Status']."','".$row1['Copy_Status_Eff_Date']."','".$row1['Custodian']."','".$row1['Badge_Number']."','".$row1['Parts_Class']."','".$row1['Parts_Status']."','".$row1['Parts_Status_Eff_Date']."',
                      '".$row1['Parts_Type']."','".$row1['Parts_Description']."','".$row1['Archived_Date']."','".$row1['Archived_Approver']."','".$row1['Archived_Agency']."','".$row1['Review_Date']."','".$row1['Review_Status']."','".$row1['SN']."')";
                      $result_post_not_complete_duplicates = mysqli_query($con,$sql_post_not_complete_duplicate);

                      $sql_post_not_complete_duplicate_space = "INSERT into not_complete_duplicates(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
                        DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
                        Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
                        Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
                        values ('"."In Database Item"."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                        '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                        '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                        '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."')";
                        $result_post_not_complete_duplicates_space = mysqli_query($con,$sql_post_not_complete_duplicate_space);

                      $sql_post_not_complete_duplicate_old = "INSERT into not_complete_duplicates(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
                        DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
                        Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
                        Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
                        values ('".$row2['DCC_Number']."','".$row2['Document_Class']."','".$row2['SubClass']."','".$row2['Origin_Date']."','".$row2['Task_Number']."','".$row2['TD_Number']."','".$row2['Declass_Date']."','".$row2['Document_Status']."',
                        '".$row2['Doc_Status_Eff_Date']."','".$row2['DCC_Number_Repeat']."','".$row2['Agency_Name']."','".$row2['DCC_Last_Modified_On']."','".$row2['Logged_Date']."','".$row2['Number_of_Copies']."','".$row2['Number_of_Parts']."','".$row2['Copy_Number']."','".$row2['Copy_Container']."',
                        '".$row2['Part_Number']."','".$row2['Parts_Container']."','".$row2['Copy_Status']."','".$row2['Copy_Status_Eff_Date']."','".$row2['Custodian']."','".$row2['Badge_Number']."','".$row2['Parts_Class']."','".$row2['Parts_Status']."','".$row2['Parts_Status_Eff_Date']."',
                        '".$row2['Parts_Type']."','".$row2['Parts_Description']."','".$row2['Archived_Date']."','".$row2['Archived_Approver']."','".$row2['Archived_Agency']."','".$row2['Review_Date']."','".$row2['Review_Status']."','".$row2['SN']."')";
                        $result_post_not_complete_duplicates_old = mysqli_query($con,$sql_post_not_complete_duplicate_old);

                        $sql_post_not_complete_duplicate_space = "INSERT into not_complete_duplicates(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
                          DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
                          Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
                          Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
                          values ('".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                          '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                          '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."',
                          '".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."','".$empty."')";
                          $result_post_not_complete_duplicates_space = mysqli_query($con,$sql_post_not_complete_duplicate_space);
              }
            }
          }
        }
      }
    }

    //posting all data to permanet data that isn't a complete duplicate.
    //also posting data that didn't have a repeat sn or DCC
    //$duplicate_repeat_sn_dcc_new, $duplicate_repeat_sn_sn_new, and complete_duplicate_sn_values has all the SN's that we don't want to push to permanent_data
    if (isset($duplicate_repeat_sn_dcc_new) && isset($duplicate_repeat_sn_sn_new) && isset($complete_duplicate_sn_values)){
      $all_non_push_sn = array_merge($duplicate_repeat_sn_dcc_new,$duplicate_repeat_sn_sn_new,$complete_duplicate_sn_values);
      $sql_final_push = "SELECT * FROM csv_data";
      $result_final_push = mysqli_query($con,$sql_final_push);
      while ($row = mysqli_fetch_assoc($result_final_push)){
        if (!in_array($row['SN'],$all_non_push_sn)){
          $sql_post_final = "INSERT into permanent_data_temp(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
            DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
            Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
            Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
            values ('".$row['DCC_Number']."','".$row['Document_Class']."','".$row['SubClass']."','".$row['Origin_Date']."','".$row['Task_Number']."','".$row['TD_Number']."','".$row['Declass_Date']."','".$row['Document_Status']."',
              '".$row['Doc_Status_Eff_Date']."','".$row['DCC_Number_Repeat']."','".$row['Agency_Name']."','".$row['DCC_Last_Modified_On']."','".$row['Logged_Date']."','".$row['Number_of_Copies']."','".$row['Number_of_Parts']."','".$row['Copy_Number']."','".$row['Copy_Container']."',
              '".$row['Part_Number']."','".$row['Parts_Container']."','".$row['Copy_Status']."','".$row['Copy_Status_Eff_Date']."','".$row['Custodian']."','".$row['Badge_Number']."','".$row['Parts_Class']."','".$row['Parts_Status']."','".$row['Parts_Status_Eff_Date']."',
              '".$row['Parts_Type']."','".$row['Parts_Description']."','".$row['Archived_Date']."','".$row['Archived_Approver']."','".$row['Archived_Agency']."','".$row['Review_Date']."','".$row['Review_Status']."','".$row['SN']."')";
              $result_post_final = mysqli_query($con,$sql_post_final);
        }
      }
    }

    ?>
  </body>
</html>
