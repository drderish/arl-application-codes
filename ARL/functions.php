<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
    include_once 'server_connection.php';

     if(isset($_POST["Import"])){

        $filename=$_FILES["file"]["tmp_name"];
         if($_FILES["file"]["size"] > 0)
         {
           $SN_array = array();
           $repeating_SN_values = array();
            $file = fopen($filename, "r");
              while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
               {
                 // create the SN finder here so that it can be added to the data at this point
                 $reduced_data = str_replace(' ','',$getData[27]);
                 $counter1 = 0;
                 $counter2 = 3;
                 while ($counter2 < strlen($reduced_data) - 8){
                   if (substr($reduced_data,$counter1,4) == 'S/N:'){
                     $SN_temp = substr($reduced_data,$counter2+1,8);
                     if(in_array($SN_temp,$SN_array)){
                       $repeating_SN_values[] = $SN_temp;
                     }
                     $SN_array[] = $SN_temp;
                   }
                   $counter1 ++;
                   $counter2 ++;
                 }
                 $sql = "INSERT into csv_data(DCC_Number,Document_Class,SubClass,Origin_Date,Task_Number,TD_Number,Declass_Date,Document_Status,Doc_Status_Eff_Date,
                 DCC_Number_Repeat,Agency_Name,DCC_Last_Modified_On,Logged_Date,Number_of_Copies,Number_of_Parts,Copy_Number,
                 Copy_Container,Part_Number,Parts_Container,Copy_Status,Copy_Status_Eff_Date,Custodian,Badge_Number,Parts_Class,Parts_Status,
                 Parts_Status_Eff_Date,Parts_Type,Parts_Description,Archived_Date,Archived_Approver,Archived_Agency,Review_Date,Review_Status,SN)
                 values ('".$getData[0]."','".$getData[1]."','".$getData[2]."','".$getData[3]."',
                       '".$getData[4]."','".$getData[5]."','".$getData[6]."','".$getData[7]."','".$getData[8]."',
                       '".$getData[0]."','".$getData[10]."','".$getData[11]."','".$getData[12]."','".$getData[13]."',
                       '".$getData[14]."','".$getData[15]."','".$getData[16]."','".$getData[17]."','".$getData[18]."',
                       '".$getData[19]."','".$getData[20]."','".$getData[21]."','".$getData[22]."','".$getData[23]."',
                       '".$getData[24]."','".$getData[25]."','".$getData[26]."','".$getData[27]."','".$getData[28]."',
                       '".$getData[29]."','".$getData[30]."','".$getData[31]."','".$getData[32]."','".$SN_temp."')";
                       $result = mysqli_query($con, $sql);
        if (count($repeating_SN_values) > 0){
          echo "Atleast Two Items That Were Submitted, Had The Same SN Values."

          <form  action="index.php">
          <input type="submit" value="Return To Submission Page.">
          </form>

          session_start();
          $_SESSION['repeat_sn'] = $repeating_SN_values;
          <form  action="display_repeat.php">
          <input type="submit" value="See Which Items Have Duplicate SN Values.">
          </form>
        }else{
            if(!isset($result))
            {
              echo "<script type=\"text/javascript\">
                  alert(\"Invalid File:Please Upload CSV File.\");
                  window.location = \"index.php\"
                  </script>";
            }
            else {
                echo "<script type=\"text/javascript\">
                alert(\"CSV File has been successfully Imported.\");
                window.location = \"tables.php\"
              </script>";
           }
         }
        }

               fclose($file);
      }
    }
     ?>
  </body>
</html>
