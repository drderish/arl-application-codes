<?php
$servername = "localhost";
$username = "root";
$password = "";
$db_conect = mysqli_connect($servername,$username,$password);
$databases = array_column(mysqli_fetch_all($db_conect->query('SHOW DATABASES')),0);
if(!in_array("final_test",$databases)){
  mysqli_query("CREATE DATABASE final_test");
}
$db = "final_test";
$con = mysqli_connect($servername, $username, $password, $db);
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <form action="functions.php" method="POST" enctype="multipart/form-data">
    <input type="file" name="file" placeholder="file">
    <br>
    <button type = "submit" id="submit" name="Import">Submit CSV File</button>
    </form>
    <?php
    $tables = array_column(mysqli_fetch_all($con->query('SHOW TABLES')),0);
    $table_names = ["complete_duplicates","csv_data","duplicate_dcc_sn_new","duplicate_repeat_dcc_new","duplicate_repeat_sn_new"
    ,"duplicate_sn_dcc_old","duplicate_sn_old","not_complete_duplicates","permanent_data","permanent_data_temp","duplicate_sn_new"];
    foreach($table_names as $table_name){
      if(!in_array($table_name,$tables)){
        $sql = "CREATE TABLE $table_name(
    	  id int(11) not null PRIMARY KEY AUTO_INCREMENT,
        DCC_Number text(1000) null,
        Document_Class text(1000) null,
        SubClass text(1000) null,
        Origin_Date text(1000) null,
        Task_Number text(1000) null,
        TD_Number text(1000) null,
        Declass_Date text(1000) null,
        Document_Status text(1000) null,
        Doc_Status_Eff_Date text(1000) null,
        DCC_Number_Repeat text(1000) null,
        Agency_Name text(1000) null,
        DCC_Last_Modified_On text(1000) null,
        Logged_Date text(1000) null,
        Number_of_Copies text(1000) null,
        Number_of_Parts text(1000) null,
        Copy_Number text(1000) null,
        Copy_Container text(1000) null,
        Part_Number text(1000) null,
        Parts_Container text(1000) null,
        Copy_Status text(1000) null,
        Copy_Status_Eff_Date text(1000) null,
        Custodian text(1000) null,
        Badge_Number text(1000) null,
        Parts_Class text(1000) null,
        Parts_Status text(1000) null,
        Parts_Status_Eff_Date text(1000) null,
        Parts_Type text(1000) null,
        Parts_Description text(1000) null,
        Archived_Date text(1000) null,
        Archived_Approver text(1000) null,
        Archived_Agency text(1000) null,
        Review_Date text(1000) null,
        Review_Status text(1000) null,
    	  SN text(1000) null
        )";
        mysqli_query($con,$sql);
      }
    }
    foreach($table_names as $table_name){
      $sql="TRUNCATE TABLE $table_name";
      mysqli_query($con,$sql);
    ?>

  </body>
</html>
