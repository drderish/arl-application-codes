function true_anom = kepeqtnE(M,e)
error = 1;
    if M < pi
        Eo = M + e/2;
    elseif M > pi
        Eo = M - e/2;
    else
        Eo = pi;
    end
    while error > .000001
        F = Eo - e*sin(Eo) - M;
        F_prime = 1 - e*cos(Eo);
        E_new = Eo - F/F_prime;
        error = abs(E_new - Eo);
        Eo = E_new;
    end
true_anom = 2*atan2(sqrt(1+e)*tan(Eo/2),sqrt(1-e));
end