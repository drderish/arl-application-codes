function dfdt = rates(t,f,mu_earth,mu_sun)
r_sun_km_J2000 = JDTDB_to_rGCRF(t);
x = f(1);
y = f(2);
z = f(3);
vx = f(4);
vy = f(5);
vz = f(6);
rsat = [x y z];
rsat_sun = r_sun_km_J2000 - rsat;
r = norm([x y z]);
ax = -mu_earth*x/r^3 + mu_sun*((rsat_sun(1)/(norm(rsat_sun)^3) - (r_sun_km_J2000(1)/(norm(r_sun_km_J2000)^3))));
ay = -mu_earth*y/r^3 + mu_sun*((rsat_sun(2)/(norm(rsat_sun)^3) - (r_sun_km_J2000(2)/(norm(r_sun_km_J2000)^3))));
az = -mu_earth*z/r^3 + mu_sun*((rsat_sun(3)/(norm(rsat_sun)^3) - (r_sun_km_J2000(3)/(norm(r_sun_km_J2000)^3))));
dfdt = [vx vy vz ax ay az]';
return