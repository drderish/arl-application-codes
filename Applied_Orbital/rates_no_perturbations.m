function dfdt = rates_no_perturbations(t,f_unperturbed)
mu_earth = 398600.4415;
x = f_unperturbed(1);
y = f_unperturbed(2);
z = f_unperturbed(3);
vx = f_unperturbed(4);
vy = f_unperturbed(5);
vz = f_unperturbed(6);
r = norm([x y z]);
ax = -mu_earth*x/r^3;
ay = -mu_earth*y/r^3;
az = -mu_earth*z/r^3;
dfdt = [vx vy vz ax ay az]';
return