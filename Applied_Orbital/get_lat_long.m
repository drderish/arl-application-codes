function [latitudes, longitudes] = get_lat_long()
    states_info = readtable('statelatlong.csv');
    latitudes = table2array(states_info(:,2));
    longitudes = table2array(states_info(:,3));
end