clear all
format long
orbit_count = 1;
a = 42164;
e = .22;
inclination = degrees_to_rad(63.4);
time_span = linspace(0,86164.1*orbit_count,86164.1*orbit_count/400);
OMEGA1 = degrees_to_rad(-5.65);
OMEGA2 = degrees_to_rad(-102.1);
OMEGA3 = degrees_to_rad(-269.1);
w = degrees_to_rad(-90);
mu_earth = 398600.4415;
p = a*(1-e^2);

v1 = 0;
[rijk1,vijk1] = coe2rv(p,e,inclination,OMEGA1,w,v1,mu_earth);
v2 = 120*(pi/180);
[rijk2,vijk2] = coe2rv(p,e,inclination,OMEGA2,w,v2,mu_earth);
v3 = 240*(pi/180);
[rijk3,vijk3] = coe2rv(p,e,inclination,OMEGA3,w,v3,mu_earth);

Year = 2020;
Month = 4;
Day = 23;
Hour = 21;
Minute = 11;
Second = 51;
JDUTC_initial = 367*Year - floor(7*(Year + floor((Month + 9)/12))/4) + floor(275*Month/9) + Day + 1721013.5 + (1/24)*(Hour + (1/60)*(Minute + Second/60));
leap_seconds = 37;
JDTDB_initial = JDUTC_initial + (leap_seconds + 32.184)/86400;
yo1 = [rijk1 vijk1];
yo2 = [rijk2 vijk2];
yo3 = [rijk3 vijk3];
myoptions = odeset('RelTol',1e-12, 'AbsTol', 1e-12);
[t,f1] = ode45(@rates_perturbations,time_span,yo1,myoptions,JDTDB_initial);
[t,f2] = ode45(@rates_perturbations,time_span,yo2,myoptions,JDTDB_initial);
[t,f3] = ode45(@rates_perturbations,time_span,yo3,myoptions,JDTDB_initial);


r_sat1_GCRF_perturbed = zeros(length(f1),3);
r_sat2_GCRF_perturbed = zeros(length(f2),3);
r_sat3_GCRF_perturbed = zeros(length(f3),3);

for i = 1:length(f1)
r_sat1_GCRF_perturbed(i,1:3) = f1(i,1:3);
end
for i = 1:length(f2)
r_sat2_GCRF_perturbed(i,1:3) = f2(i,1:3);
end
for i = 1:length(f3)
r_sat3_GCRF_perturbed(i,1:3) = f3(i,1:3);
end

thetaGo = 0;

%GCRF to ECF
thetaG = zeros(1,length(t));
wearth = 7.2921158553e-5;
for i = 1:length(t)
    thetaG(i) = thetaGo + wearth*(t(i) - t(1));
end

rECF1 = zeros(3,length(r_sat1_GCRF_perturbed));
rECF2 = zeros(3,length(r_sat2_GCRF_perturbed));
rECF3 = zeros(3,length(r_sat3_GCRF_perturbed));

for i = 1:length(t)
    QECF = [cos(thetaG(i)) sin(thetaG(i)) 0; -sin(thetaG(i)) cos(thetaG(i)) 0; 0 0 1];
    rECF1(:,i) = QECF*r_sat1_GCRF_perturbed(i,:)';
end
for i = 1:length(t)
    QECF = [cos(thetaG(i)) sin(thetaG(i)) 0; -sin(thetaG(i)) cos(thetaG(i)) 0; 0 0 1];
    rECF2(:,i) = QECF*r_sat2_GCRF_perturbed(i,:)';
end
for i = 1:length(t)
    QECF = [cos(thetaG(i)) sin(thetaG(i)) 0; -sin(thetaG(i)) cos(thetaG(i)) 0; 0 0 1];
    rECF3(:,i) = QECF*r_sat3_GCRF_perturbed(i,:)';
end

% Generating latitude and longitude
f = 3.35*10^-3;
thetagc1 = zeros(1,length(t));
thetagc2 = zeros(1,length(t));
thetagc3 = zeros(1,length(t));
for i = 1:length(t)
    thetagc1(i) = asin(rECF1(3,i)/(norm(rECF1(:,i))));
end
for i = 1:length(t)
    thetagc2(i) = asin(rECF2(3,i)/(norm(rECF2(:,i))));
end
for i = 1:length(t)
    thetagc3(i) = asin(rECF3(3,i)/(norm(rECF3(:,i))));
end

longitude1 = zeros(1,length(t));
longitude2 = zeros(1,length(t));
longitude3 = zeros(1,length(t));
for i = 1:length(t)
    longitude1(i) = atan2(rECF1(2,i),rECF1(1,i));
end
for i = 1:length(t)
    longitude2(i) = atan2(rECF2(2,i),rECF2(1,i));
end
for i = 1:length(t)
    longitude3(i) = atan2(rECF3(2,i),rECF3(1,i));
end

thetagd1 = zeros(1,length(t));
thetagd2 = zeros(1,length(t));
thetagd3 = zeros(1,length(t));
for i = 1:length(t)
    thetagd1(i) = atan2(tan(thetagc1(i)),(1-f)^2);
end
for i = 1:length(t)
    thetagd2(i) = atan2(tan(thetagc2(i)),(1-f)^2);
end
for i = 1:length(t)
    thetagd3(i) = atan2(tan(thetagc3(i)),(1-f)^2);
end

thetagd1 = thetagd1*(180/pi);
thetagd2 = thetagd2*(180/pi);
thetagd3 = thetagd3*(180/pi);
longitude1 = longitude1*(180/pi);
longitude2 = longitude2*(180/pi);
longitude3 = longitude3*(180/pi);

figure(1)
C = {'b'};
load earth_coastline.mat
plot(earth_coastline(:,1),earth_coastline(:,2),'k')
hold on
for i = 1:length(t)
    plot(longitude1(i),thetagd1(i),'k','marker','.')
    hold on
end
for i = 1:length(t)
    plot(longitude2(i),thetagd2(i),'r','marker','s')
    hold on
end
for i = 1:length(t)
    plot(longitude3(i),thetagd3(i),'g','marker','*')
    hold on
end

legend({'Satelite 1','Satelite 2','Satelite 3'},'Location','southeast','Orientation','horizontal')

set( gca, 'fontweight','bold','fontsize', 22 )
xlim([-180 180])
ylim([-90 90])
title('Ground Track of Orbit')
xlabel('Longitude [degrees]')
ylabel('Geodetic Latitude [degrees]')

hold off
[latitudes_states, longitudes_states] = get_lat_long();

latitude_errors1 = zeros(length(t),length(latitudes_states));
for ii = 1:length(t)
    for jj = 1:length(longitudes_states)
        latitude_errors1(ii,jj) = abs(thetagd1(ii) - latitudes_states(jj));
    end
end

latitude_errors2 = zeros(length(t),length(latitudes_states));
for ii = 1:length(t)
    for jj = 1:length(longitudes_states)
        latitude_errors2(ii,jj) = abs(thetagd2(ii) - latitudes_states(jj));
    end
end
latitude_errors3 = zeros(length(t),length(latitudes_states));
for ii = 1:length(t)
    for jj = 1:length(longitudes_states)
        latitude_errors3(ii,jj) = abs(thetagd3(ii) - latitudes_states(jj));
    end
end

smallest_states_error_latitude = zeros(length(t),length(latitudes_states));
for ii = 1:length(t)
    for jj = 1:length(latitudes_states)
        smallest_states_error_latitude(ii,jj) = min([latitude_errors1(ii,jj) latitude_errors2(ii,jj) latitude_errors3(ii,jj)]);
    end
end

largest_states_error_latitude_time = zeros(length(t),1);
for ii = 1:length(t)
    largest_states_error_latitude_time(ii) = max(smallest_states_error_latitude(ii,:));
end

meanlongitudes1 = zeros(orbit_count,1);
meanlatitudes1 = zeros(orbit_count,1);
meanlongitudes2 = zeros(orbit_count,1);
meanlatitudes2 = zeros(orbit_count,1);
meanlongitudes3 = zeros(orbit_count,1);
meanlatitudes3 = zeros(orbit_count,1);
for ii = 1:orbit_count
    meanlongitudes1(ii) = mean(longitude1(((215*(ii - 1)) + 1):(215*(ii))));
    meanlongitudes2(ii) = mean(longitude2(((215*(ii - 1)) + 1):(215*(ii))));
    meanlongitudes3(ii) = mean(longitude3(((215*(ii - 1)) + 1):(215*(ii))));
    meanlatitudes1(ii) = mean(thetagd1(((215*(ii - 1)) + 1):(215*(ii))));
    meanlatitudes2(ii) = mean(thetagd2(((215*(ii - 1)) + 1):(215*(ii))));
    meanlatitudes3(ii) = mean(thetagd3(((215*(ii - 1)) + 1):(215*(ii))));
end

orbit_number = linspace(1,orbit_count,orbit_count);
orbit_number_to_days = orbit_number*86164.1/86400;

figure(2)
hold on
plot(orbit_number_to_days(:),abs(meanlongitudes1(:) + 95.7),'s')
hold on
plot(orbit_number_to_days(:),abs(meanlongitudes2(:) + 95.7),'*')
hold on
plot(orbit_number_to_days(:),abs(meanlongitudes3(:) + 95.7),'o')

set( gca, 'fontweight','bold','fontsize', 22 )
title('abs(Mean Longitude Per Orbit - Longitude of US) vs Days Sense Start', 'fontsize', 20)  
ylabel( 'Difference [deg]','fontweight','bold', 'fontsize', 18  )
xlabel( 'Time [Days]','fontweight','bold', 'fontsize', 18 )
legend({'Satelite 1','Satelite 2','Satelite 3'},'Location','southeast','Orientation','horizontal')

t_days = t/86400;
max_lat = zeros(length(t),1);
for ii = 1:length(t)
    max_lat(ii) = max(largest_states_error_latitude_time(ii,:));
end

figure(3)
scatter(t_days(:),max_lat(:))

set( gca, 'fontweight','bold','fontsize', 22 )
title('Largest Latitude Difference vs Time', 'fontsize', 20)  
ylabel( 'Latitude Difference [deg]','fontweight','bold', 'fontsize', 18  )
xlabel( 'Time [Days]','fontweight','bold', 'fontsize', 18 )
legend({'Max Latitude Difference'},'Location','southeast','Orientation','horizontal')
ylim([5 50])

        