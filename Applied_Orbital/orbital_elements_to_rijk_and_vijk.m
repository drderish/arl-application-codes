function [rijk,vijk] = orbital_elements_to_rijk_and_vijk(a,e,i,omega,w,mu,true_anom)
% Function turns orbital elements in ijk position and velocity
    rp = a*(1-e);
    vp = sqrt(mu*(2/rp - 1/a));
    h = rp*vp;
    ro = h^2/(mu);
    % Perifocal frame
    dummy = (ro/(1 + e*cos(true_anom)));
    rPQW = [dummy*cos(true_anom), dummy*sin(true_anom), 0];
    vPQW = [mu/h*(-sin(true_anom)), mu/h*(e + cos(true_anom)), 0];
    Q = rotation_matrix_ijk_to_PQW(w,i,omega,e);
    Q = Q';
    rijk = Q*rPQW';
    vijk = Q*vPQW';
end