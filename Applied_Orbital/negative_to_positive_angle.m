function positive_angle = negative_to_positive_angle(negative_angle)
%angle must be in radians
if negative_angle < 0
    positive_angle = negative_angle + 2*pi;
else
    positive_angle = negative_angle;
end

