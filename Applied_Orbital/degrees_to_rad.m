function angle = degrees_to_rad(angle1)
angle = angle1*(pi/180);
end