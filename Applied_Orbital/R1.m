function Q1 = R1(o)
Q1 = [1 0 0; 0 cos(o) sin(o); 0 -sin(o) cos(o)];
end
