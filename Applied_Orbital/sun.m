function r_sun_km_J200 = sun(t)
JDUTC = 2457700 + t;
JDUT1 = JDUTC + 0; %Only plus 0 because we assume delta UT1 was 0 in this problem
JDTAI = JDUTC + 37/(86400);
JDTT = JDTAI + 32.184/(86400);
TTT = (JDTT -2451545)/(36525);

% Converting MOD to GCRF
theta_1 = 2306.2181*TTT + .30188*TTT^2 + .017998*TTT^3;
theta_1_degrees = theta_1/3600;
theta_1_rad = theta_1_degrees*(pi/180);

theta_2 = 2306.2181*TTT + .30188*TTT^2 + .017998*TTT^3;
theta_2_degrees = theta_2/3600;
theta_2_rad = theta_2_degrees*(pi/180);

theta_3 = 2306.2181*TTT + .30188*TTT^2 + .017998*TTT^3;
theta_3_degrees = theta_3/3600;
theta_3_rad = theta_3_degrees*(pi/180);

QMOD_GCRF = R3(theta_1_rad)*R2(-theta_2_rad)*R3(theta_3_rad);

%Solving for suns position
for i = 1:length(JDUTC)
TUT1 = (JDUT1 - 2451545)/(36525);
TDB = TUT1; %assumption we are making
longitude_M_sun = 280.460 + 36000.771*TDB;
M_sun = 357.52772333 + 35999.0534*TDB;
longitude_ecliptic = longitude_M_sun + 1.914555471*sind(M_sun) + .019994643*sind(2*M_sun);
r_sun_mag_AU = 1.000140612 - .016708617*cosd(M_sun) - .000139589*cosd(2*M_sun);
E = 23.439291 - .0130042*TDB;
Q_sun = [cosd(longitude_ecliptic);cosd(E)*sind(longitude_ecliptic);sind(E)*sind(longitude_ecliptic)];
%r_sun_AU_vector = r_sun_mag_AU*Q_sun;
r_sun_km_vector = r_sun_mag_AU*Q_sun*1.496*10^8;
r_sun_km_vector_GCRF = r_sun_km_vector'*QMOD_GCRF;
%r_sun_AU_vector_GCRF(1:3,i) = r_sun_AU_vector'*QMOD_GCRF(1:3,3*i-2:3*i);
end

%Creating the J200 to GCRF transition matrix
J200_GCRF_angel_1_rad = .0146/3600;
J200_GCRF_angel_2_rad = -.16617/3600;
J200_GCRF_angel_3_rad = -.0068192/3600;

QJ200_GCRF = R3(-J200_GCRF_angel_1_rad)*R2(J200_GCRF_angel_2_rad)*R1(J200_GCRF_angel_3_rad);
QGCRF_J200 = QJ200_GCRF';

%creating J200 frame vectors
r_sun_km_J200 = r_sun_km_vector_GCRF*QGCRF_J200;
end
