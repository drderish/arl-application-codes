function [a, e, i, OMEGA, w_squiggly, lambda_M] = TDB_orbital_elements_earth(TTDB)
%a-(semimajor axis) e-(eccentricity) i-(inclination) OMEGA-(Right Ascension
%of the Ascending Node) find other terms on pp 296-297 valado 4th edition

a = 1.0000010178;
e = .016708617 - .000042037*TTDB - .0000001236*TTDB^2 + .00000000004*TTDB^3;
i = .0130546*TTDB - .00000931*TTDB^2 -.000000034*TTDB^3;
OMEGA = 174.873174 - .2410908*TTDB + .00004067*TTDB^2 - .000001327*TTDB^3;
w_squiggly = 102.937348 + .3225557*TTDB + .00015026*TTDB^2 + .000000478*TTDB^3;
lambda_M = 100.466449 + 35999.3728519*TTDB - .00000568*TTDB^2;

a = a*1.495978707*10e8;

i = degrees_to_rad(i);
OMEGA = degrees_to_rad(OMEGA);
lambda_M = degrees_to_rad(lambda_M);
w_squiggly = degrees_to_rad(w_squiggly);

i = wrapTo2Pi(i);
w_squiggly = wrapTo2Pi(w_squiggly);
lambda_M = wrapTo2Pi(lambda_M);
OMEGA = wrapTo2Pi(OMEGA);
