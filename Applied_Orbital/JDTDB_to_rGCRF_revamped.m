function rGCRF = JDTDB_to_rGCRF_revamped(JDTDB_initial,t)

JDTDB = JDTDB_initial + t/86400;
TTDB = (JDTDB - 2451545)/36525;
[a, e, i, OMEGA, w_squiggly, lambda_M] = TDB_orbital_elements_earth(TTDB);

M = lambda_M - w_squiggly;
w = w_squiggly - OMEGA;
M = wrapTo2Pi(M);
w = wrapTo2Pi(w);
M = negative_to_positive_angle(M);
w = negative_to_positive_angle(w);

true_anom = kepeqtnE(M,e);
true_anom = true_anom*(180/pi);
true_anom = wrapTo360(true_anom);
true_anom = degrees_to_rad(true_anom);
mu = 1;

p = a*(1-e^2);
[rijk, vijk] = coe2rv(p,e,i,OMEGA,w,true_anom,mu,w_squiggly,lambda_M);
vijk = vijk/58.1324409;

ecliptic_angle = 23.439291 - .0130042*TTDB;
ecliptic_angle = degrees_to_rad(ecliptic_angle);
Q = R1(-ecliptic_angle);

rJ2000 = Q*rijk;
vJ2000 = Q*vijk;

QGCRF_J2000 = Get_QGCRF_J200();
QJ2000_GCRF = QGCRF_J2000';

rGCRF = QJ2000_GCRF*rJ2000;
vGCRF = QJ2000_GCRF*vJ2000;
end