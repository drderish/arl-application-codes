function [rijk, vijk] = coe2rv(p,e,inclination,OMEGA,w,v,mu,wtrue,Lambda_true)
if e < 1*10^-12 && inclination < 1*10^-12
    w = 0;
    OMEGA = 0;
    v = Lambda_true;
elseif e < 1*10^-12 && inclination > 1*10^-12
    w = 0;
    v = u;

elseif e > 1*10^-12 && inclination < 1*10^-12
    OMEGA = 0; 
    w = wtrue;
end

rPQW = [p*cos(v)/(1+e*cos(v)); p*sin(v)/(1+e*cos(v)); 0];
vPQW = [-sqrt(mu/p)*sin(v); sqrt(mu/p)*(e + cos(v)); 0];

Q = R3(-OMEGA)*R1(-inclination)*R3(-w);

rijk = Q*rPQW;
vijk = Q*vPQW;