function [dfdt] = rates_perturbations(t,f,JDTDB_initial)
%Evaluated in the GCRF frame or ijk frame
%Just need to check sun position equation
J2 = 0.0010826267;
J3 = -.0000025327;
Rearth = 6378.1363;
mu_earth = 398600.4415;
mu_sun = 1.327*10^11;
mu_moon = 3903;
earth_rotation = [0 0 7.2921158553e-5];
CD = 2;
A_m = .01;
CR = 1.5;
x_unit = [1 0 0];
y_unit = [0 1 0];
z_unit = [0 0 1];

x = f(1);
y = f(2);
z = f(3);
vx = f(4);
vy = f(5);
vz = f(6);
r = norm([x y z]);

% Solar Radiation Pressure Perturbation
r_sun_to_earth_km_GCRF = JDTDB_to_rGCRF_revamped(JDTDB_initial,t);
rsat = [x y z]';
r_sun_to_sat = r_sun_to_earth_km_GCRF + rsat;
rsat_sun = -r_sun_to_sat;
r_earth_to_sun_km_GCRF = -r_sun_to_earth_km_GCRF;
gamma = get_gamma(r_earth_to_sun_km_GCRF,rsat);
aSRP = (-4.57*10^-9)*gamma*(CR*A_m)*(rsat_sun)/(norm(rsat_sun));
ax_SRP = dot(aSRP,x_unit);
ay_SRP = dot(aSRP,y_unit);
az_SRP = dot(aSRP,z_unit);
%  
% Drag due to earths atmosphere perturbation
r_vector = [x y z];
v_vector = [vx vy vz];
density = get_density(r);
v_rel = v_vector - cross(earth_rotation,r_vector);
a_drag = -1000*(1/2)*(CD*A_m)*density*norm(v_rel)*v_rel;
a_drag = a_drag';
ax_drag = dot(a_drag,x_unit);
ay_drag = dot(a_drag,y_unit);
az_drag = dot(a_drag,z_unit);
 
% Perturbation due to Moon
JD_TDB = JDTDB_initial + t/86400;
r_earth_moon_GCRF = Moon( JD_TDB );
r_spacecraft_moon = r_earth_moon_GCRF - rsat;
moon_perturbation = mu_moon*(r_spacecraft_moon/(norm(r_spacecraft_moon)^3) - r_earth_moon_GCRF/(norm(r_earth_moon_GCRF)^3));
ax_moon = dot(moon_perturbation,x_unit);
ay_moon = dot(moon_perturbation,y_unit);
az_moon = dot(moon_perturbation,z_unit);

ax_J2_J3 = ((3*mu_earth*J2*Rearth^2)/(2*r^5))*x*(5*(z/r)^2 -1) -(5*J3*mu_earth*Rearth^3/(2*r^7))*(x*(3*z - 7*z^3/(r^2)));
ay_J2_J3 = ((3*mu_earth*J2*Rearth^2)/(2*r^5))*y*(5*(z/r)^2 -1) -(5*J3*mu_earth*Rearth^3/(2*r^7))*(y*(3*z - 7*z^3/(r^2)));
az_J2_J3 = ((3*mu_earth*J2*Rearth^2)/(2*r^5))*z*(5*(z/r)^2 -3) -(5*J3*mu_earth*Rearth^3/(2*r^7))*(6*z^2 - (7*z^4)/(r^2) - (3/5)*r^2);

ax_Sun = mu_sun*(rsat_sun(1)/(norm(rsat_sun)^3) - (-r_sun_to_earth_km_GCRF(1)/(norm(r_sun_to_earth_km_GCRF)^3)));
ay_Sun = mu_sun*(rsat_sun(2)/(norm(rsat_sun)^3) - (-r_sun_to_earth_km_GCRF(2)/(norm(r_sun_to_earth_km_GCRF)^3)));
az_Sun = mu_sun*(rsat_sun(3)/(norm(rsat_sun)^3) - (-r_sun_to_earth_km_GCRF(3)/(norm(r_sun_to_earth_km_GCRF)^3)));

ax = -mu_earth*x/r^3 + ax_J2_J3 + ax_Sun + ax_drag + ax_SRP + ax_moon;
ay = -mu_earth*y/r^3 + ay_J2_J3 + ay_Sun + ay_drag + ay_SRP + ay_moon;
az = -mu_earth*z/r^3 + az_J2_J3 + az_Sun + az_drag + az_SRP + az_moon;

dfdt = [vx vy vz ax ay az]';
end