function [Q] = rotation_matrix_ijk_to_PQW(angle1,angle2,angle3,e)
%Creates rotation matrix for a R3R1R3 sequence ijk to PQW frame angles must
%go in the order of lower case omega, inclination, uppercase omega
    if e == 0 || angle2 == 0
        Q = eye(3);
    elseif e == 0
        R3 = [cos(-angle3) sin(-angle3) 0;-sin(-angle3) cos(-angle3) 0; 0 0 1];
        R1 = [1 0 0; 0 cos(-angle2) sin(-angle2); 0 -sin(-angle2) cos(-angle2)];
        Q = R3*R1;
        Q = Q';
    elseif angle2 == 0
        Q = [cos(-angle1) sin(-angle1) 0;-sin(-angle1) cos(-angle1) 0; 0 0 1];
        Q = Q';
    else   
        R31 = [cos(angle1) sin(angle1) 0;-sin(angle1) cos(angle1) 0; 0 0 1];
        R12 = [1 0 0; 0 cos(angle2) sin(angle2); 0 -sin(angle2) cos(angle2)];
        R33 = [cos(angle3) sin(angle3) 0;-sin(angle3) cos(angle3) 0; 0 0 1];
        Q = R31*R12*R33;
    end
end