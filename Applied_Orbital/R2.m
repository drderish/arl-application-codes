function Q2 = R2(o)
Q2 = [cos(o) 0 -sin(o); 0 1 0; sin(o) 0 cos(o)];
end