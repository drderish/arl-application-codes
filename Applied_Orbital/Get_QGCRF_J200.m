function QGCRF_J200 = Get_QGCRF_J200()
%outputs a matrix that transforms GCRF vectors to J200
J200_GCRF_angel_1_rad = .0146/3600*(pi/180);
J200_GCRF_angel_2_rad = -.16617/3600*(pi/180);
J200_GCRF_angel_3_rad = -.0068192/3600*(pi/180);
QJ200_GCRF = R3(-J200_GCRF_angel_1_rad)*R2(J200_GCRF_angel_2_rad)*R1(J200_GCRF_angel_3_rad);
QGCRF_J200 = QJ200_GCRF';
end