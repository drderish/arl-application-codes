function gamma = get_gamma(rearthsun,rs)
%takes the vectors of earth to sun and sun to spacecraft and calculates the
%value of gamma. gamma represents the amount of sun light that is visible by the
%spacecraft. 
Rearth = 6378.1363;
Rsun = 696340;
rearthsununit = rearthsun/(norm(rearthsun));
ro = -dot(rs,rearthsununit);
l = sqrt(norm(rs)^2 - ro^2);
Bp = (Rearth + Rsun)/norm(rearthsun);
c1 = ro + Rearth/Bp;
lp = c1*tan(Bp);
Bu = (Rsun - Rearth)/norm(rearthsun);
c2 = ro - Rearth/Bu;
lu = c2*tan(Bu);

if lp < l
    gamma = 1;
elseif l < abs(lu)
    gamma = 0;
else
    rsunsc = rs - rearthsun;
    a = asin(Rsun/norm(rsunsc));
    b = asin(Rearth/norm(rs));
    c = acos(dot(rs,rsunsc)/(norm(rs)*norm(rsunsc)));
    x = (c^2 + a^2 - b^2)/(2*c);
    y = sqrt(a^2 - x^2);
    A = a^2*acos(x/a) + b^2*acos((c-x)/b) - c*y;
    gamma = 1 - A/(pi*a^2);
end

