function [a,e,i,omega,w,true_anom] = rijk_vijk_to_orbital_elements(rijk,vijk)
mu_earth = 398600;
r_mag = norm(rijk);
v_mag = norm(vijk);
eccentricity = (1/mu_earth)*((v_mag^2 - mu_earth/r_mag)*rijk - (dot(rijk,vijk)*vijk));
e = norm(eccentricity);
h_vector = cross(rijk,vijk);
h_mag = norm(h_vector);
ro = h_mag^2/mu_earth;
rp = ro/(1+e);
a = rp/(1-e);
k = [0 0 1];
i = acos(dot(h_vector,k)/h_mag);
n = cross(k,h_vector);
omega = acos(n(1)/norm(n));
if n(2) < 0 
    omega = 2*pi - omega;
end
w = acos(dot(n,eccentricity)/(e*norm(n)));
if eccentricity(3) < 0
    w = 2*pi - w;
end
true_anom = acos(dot(rijk,eccentricity)/(r_mag*e));
if dot(rijk,vijk) < 0
    true_anom = 2*pi - true_anom;
end
