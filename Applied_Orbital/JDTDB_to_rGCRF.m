function rGCRF = JDTDB_to_rGCRF(JDTDB)
%finds the vector between the sun and earth in [AU]
TTDB = (JDTDB - 2451545)/36525;
[a, e, i, OMEGA, w_squiggly, lambda_M] = TDB_orbital_elements_earth(TTDB);

M = lambda_M - w_squiggly;
w = w_squiggly - OMEGA;
M = wrapTo360(M);
 
i = degrees_to_rad(i);
OMEGA = degrees_to_rad(OMEGA);
w = degrees_to_rad(w);
lambda_M = degrees_to_rad(lambda_M);
M = degrees_to_rad(M);
w_squiggly = degrees_to_rad(w_squiggly);

true_anom = kepeqtnE(M,e);
true_anom = true_anom*(180/pi);
true_anom = wrapTo360(true_anom);
true_anom = degrees_to_rad(true_anom);
mu = 1;

p = a*(1-e^2);
[rijk, vijk] = coe2rv(p,e,i,OMEGA,w,true_anom,mu,w_squiggly,lambda_M);
vijk = vijk/58.1324409;

TTT = TTDB;
ecliptic_angle = 23.429279 - .0130102*TTT - (5.086*10^(-8))*TTT^2 + (5.565*10^-7)*TTT^3 + 1.6*10^(-10)*TTT^4 + 1.21*10^-11*TTT^5;
ecliptic_angle = degrees_to_rad(ecliptic_angle);
Q = R1(-ecliptic_angle);
rJ2000 = Q*rijk;
vJ2000 = Q*vijk;

QGCRF_J2000 = Get_QGCRF_J200();
QJ2000_GCRF = QGCRF_J2000';

rGCRF = QJ2000_GCRF*rJ2000;
vGCRF = QJ2000_GCRF*vJ2000;

rGCRF = rGCRF*1.495978707e+11;
end