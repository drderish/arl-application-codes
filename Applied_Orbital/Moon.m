function pos_GCRF = Moon( JD_TDB )
%
% function pos_j2000 = Moon( JD_TDB )
% ---------------------------------------------------------------------
%
% Description:
%
%  Function to compute the Cartesian position of the Moon at a given time
%
% Inputs:
%
%  JD_TDB - Time of the evaluation in Julian Day format in the TDB system 
%
% Outputs:
%
%  J2000 position in geocentric equatorial coordinates
%
% Assumptions/References:
%
%  Based on the Moon algorithm (pp. 288) in Fundamentals of Astrodynamics
%  and Applications, Vallado and McClain, 4th Edition.
%
%  Assumes the radius of the Earth is 6378.1363 km when converting from Re
%  to km.

%  Pre-compute the conversion from degrees to radians
D2R = pi/180.0;

% Convert the input time into Julian Centuries
T_TDB = ( JD_TDB - 2451545.0  ) / 36525.0;

long_ecliptic= 218.32  + 481267.8813 *T_TDB ...
    + 6.29 *sind( 134.9 +477198.85 *T_TDB ) ...
    - 1.27 *sind( 259.2 -413335.38 *T_TDB ) ...
    + 0.66 *sind( 235.7 +890534.23 *T_TDB ) ...
    + 0.21 *sind( 269.9 +954397.70 *T_TDB ) ...
    - 0.19 *sind( 357.5 + 35999.05 *T_TDB ) ...
    - 0.11 *sind( 186.6 +966404.05 *T_TDB );      % deg

lat_ecliptic =   5.13 *sind( 93.3 +483202.03 *T_TDB ) ...
    + 0.28 *sind( 228.2 +960400.87 *T_TDB ) ...
    - 0.28 *sind( 318.3 +  6003.18 *T_TDB ) ...
    - 0.17 *sind( 217.6 -407332.20 *T_TDB );      % deg

parralax =  0.9508  + 0.0518 *cosd( 134.9 +477198.85 *T_TDB ) ...
    + 0.0095 *cosd( 259.2 -413335.38 *T_TDB ) ...
    + 0.0078 *cosd( 235.7 +890534.23 *T_TDB ) ...
    + 0.0028 *cosd( 269.9 +954397.70 *T_TDB );    % deg


%  Use the unwrap() function to get an angle in the range [-pi,pi]
long_ecliptic = unwrap( long_ecliptic*D2R );
lat_ecliptic  = unwrap( lat_ecliptic*D2R );
parralax      = unwrap( parralax*D2R );

obliquity = 23.439291  - 0.0130042 *T_TDB;  %deg
obliquity = obliquity * D2R;

% Pre-compute cosine and sine values that will be needed at least twice
cos_lag_ecl = cos( lat_ecliptic );
sin_lon_ecl = sin( long_ecliptic );
sin_lat_ecl = sin( lat_ecliptic );
cos_obliq   = cos( obliquity );
sin_obliq   = sin( obliquity );

% ------------- calculate moon position vector ----------------
pos_j2000 = [ cos_lag_ecl * cos(long_ecliptic); ...
              cos_obliq*cos_lag_ecl*sin_lon_ecl - sin_obliq*sin_lat_ecl;
              sin_obliq*cos_lag_ecl*sin_lon_ecl + cos_obliq*sin_lat_ecl ];
          
%  Generate position in km where Re = 6378.1363 km
pos_j2000 = pos_j2000.*(6378.1363/sin(parralax));
QGCRF_J2000 = Get_QGCRF_J200();
QJ2000_GCRF = QGCRF_J2000';
pos_GCRF = QJ2000_GCRF*pos_j2000;
