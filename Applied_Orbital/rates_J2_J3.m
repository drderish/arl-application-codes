function dfdt = rates_J2_J3(t,f,mu)
J2 = -.00108248;
Rearth = 6371;
x = f(1);
y = f(2);
z = f(3);
vx = f(4);
vy = f(5);
vz = f(6);
r = norm([x y z]);
ax = (-mu*x/r^3)*(1 - J2*(3/2)*((Rearth/r)^3)*((5*(y/r)^2)-1));
ay = (-mu*y/r^3)*(1 - J2*(3/2)*((Rearth/r)^3)*((5*(y/r)^2)-1));
az = (-mu*z/r^3)*(1 - J2*(3/2)*((Rearth/r)^3)*((5*(y/r)^2)-3));
dfdt = [vx vy vz ax ay az]';
return