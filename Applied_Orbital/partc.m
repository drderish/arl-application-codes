a = 42164;
e = .0;
inclination = 63.4*(pi/180);
w = -90*(pi/180);
o = -10*(pi/180);
truanominitial = 0*(pi/180);
thetaGo = 0*(pi/180);
u = 398600;

perioda = 2*pi*sqrt((a^3)/u);
timespan = (0:60:3*perioda);
Eo = 2*atan2(sqrt(1-e)*tan(truanominitial)/2,sqrt(1+e));
Mo = Eo - e*sin(Eo);
n = 2*pi/(perioda);
Me = zeros(1,length(timespan));
for i = 1:length(timespan)
    Me(i) = n*(timespan(i) - Mo/n);
end
Einitial = zeros(1,length(Me));
for i = 1:length(Me)
    if Me(i) < pi
        Einitial(i) = Me(i) + e/2;
    end
    if Me(i) > pi
        Einitial(i) = Me(i) - e/2;
    end
end
Eactual = zeros(1,length(Einitial));
for j = 1:length(Einitial)
    Edummy = Einitial(j);
    error = 1;
    while error > .001
        E = Edummy - (Edummy - e*sin(Edummy)- Me(j))/(1-e*cos(Edummy));
        error = abs(E - Edummy);
        Edummy = E;
    end
    Eactual(j) = Edummy;
end
trueanom = zeros(1,length(Eactual));
for i = 1:length(Eactual)
    trueanom(i) = 2*atan2(sqrt(1+e)*tan(Eactual(i)/2),sqrt(1-e));
end

% PQW FRAME
rp = a*(1-e);
vp = sqrt(u*(2/rp- 1/a));
h = rp*vp;
ro = (h^2)/u;
rP = zeros(1,length(trueanom));
rQ = zeros(1,length(trueanom));
for i = 1:length(trueanom)
    rP(i) = (ro/(1+e*cos(trueanom(i))))*cos(trueanom(i));
    rQ(i) = (ro/(1+e*cos(trueanom(i))))*sin(trueanom(i));
end
rPQW = zeros(3,length(rP));
for i = 1:length(rP)
    rPQW(1,i) = rP(i);
    rPQW(2,i) = rQ(i);
    rPQW(3,i) = 0;
end

% PQW to ijk
QPQW = [cos(-o) sin(-o) 0; -sin(-o) cos(-o) 0; 0 0 1]*[1 0 0; 0 cos(-inclination) sin(-inclination); 0 -sin(-inclination) cos(-inclination)]*[cos(-w) sin(-w) 0; -sin(-w) cos(-w) 0; 0 0 1];
rijk = zeros(3,length(rP));
for i = 1:length(rPQW)
    rijk(:,i) = QPQW*rPQW(:,i);
end

% ijk to ECF
wearth = 2*pi/(23*3600 + 56*60 + 4.1);
thetaG = zeros(1,length(timespan));
for i = 1:length(timespan)
    thetaG(i) = thetaGo + wearth*(timespan(i) - timespan(1));
end
rECF = zeros(3,length(rijk));
for i = 1:length(timespan)
    QECF = [cos(thetaG(i)) sin(thetaG(i)) 0; -sin(thetaG(i)) cos(thetaG(i)) 0; 0 0 1];
    rECF(:,i) = QECF*rijk(:,i);
end

% Generating latitude and longitude
f = 3.35*10^-3;
thetagc = zeros(1,length(timespan));
for i = 1:length(timespan)
    thetagc(i) = asin(rECF(3,i)/(norm(rECF(:,i))));
end
longitude = zeros(1,length(timespan));
for i = 1:length(timespan)
    longitude(i) = atan2(rECF(2,i),rECF(1,i));
end
thetagd = zeros(1,length(timespan));
for i = 1:length(timespan)
    thetagd(i) = atan2(tan(thetagc(i)),(1-f)^2);
end

%Plotting 

thetagd = thetagd*(180/pi);
longitude = longitude*(180/pi);
C = {'b'};
load earth_coastline.mat
plot(earth_coastline(:,1),earth_coastline(:,2),'k')
hold on
for i = 1:length(timespan)
    plot(longitude(i),thetagd(i),'color',C{1},'marker','.')
    hold on
end
hold off
xlim([-180 180])
ylim([-90 90])
title('Ground Track of Orbit [part c]')
xlabel('Longitude [degrees]')
ylabel('Geodetic Latitude [degrees]')