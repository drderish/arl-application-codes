This code will create a ground track of a satelite. The ground track will account for several perturbation, including:
-The moon
-The Sun
-Atmospheric drag
-Solare Radiation Pressure
-J2
-J3

To use this code, users need to go to main_script.m. In this file, users can choose several variables dealing with the orbit, including:
-number of orbits
-(a) which stands for semi major axis
-(e) which stands for eccentricity
-OMEGA1,OMEGA2,OMEGA3 These are the starting longitude of ascending node for the three satellites that the code is programmed to track.
-w argument of periapsis

After these values have been set, run the code and the ground track will be created.