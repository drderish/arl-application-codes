function Q3 = R3(o)
Q3 = [cos(o) sin(o) 0; -sin(o) cos(o) 0; 0 0 1];
end